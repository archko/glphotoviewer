/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.photos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import com.android.launcher3.GLImageView;

public class FullscreenViewer extends Activity {

    private GLImageView mTextureView;
    public static final String EXTRA_URL = "extra_url";
    String mPath = null;
    Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String path = null;//= "/storage/sdcard0/IMG_20150106_202851.jpg";
        Intent intent = getIntent();
        System.out.println("intent:" + getIntent());
        if (null != getIntent() && null != getIntent().getData()) {
            System.out.println("data:" + getIntent().getData());
            if (Intent.ACTION_VIEW.equals(intent.getAction())) {
                Uri uri = intent.getData();
                System.out.println("URI to open is: " + uri);
                imageUri = uri;
                /*if (uri.toString().startsWith("content://")) {
                    try {
                        Cursor cursor = getContentResolver().query(uri, new String[]{"_data"}, null, null, null);
                        if (cursor.moveToFirst()) {
                            String str = cursor.getString(0);
                            if (str == null) {
                                System.out.println("Couldn't parse data in intent");
                            } else {
                                uri = Uri.parse(str);
                                path = Uri.decode(uri.getEncodedPath());
                            }
                        }
                    } catch (Exception e2) {
                        System.out.println("Exception in Transformer Prime file manager code: " + e2);
                    }
                } else if (uri.toString().startsWith("file")) {
                    path = uri.toString().substring(7);
                }*/
            } else {
                String url = intent.getStringExtra(EXTRA_URL);
                if (TextUtils.isEmpty(url)) {
                    path = intent.getData().toString();
                } else {
                    path = url;
                }
            }
        }
        System.out.println("path:" + path);
        mTextureView = new GLImageView(this);
        setContentView(mTextureView);
        mPath = path;

        updateTextureView();
    }

    private void updateTextureView() {
        BitmapRegionTileSource.BitmapSource bitmapSource = null;
        if (!TextUtils.isEmpty(mPath)) {
            bitmapSource = new BitmapRegionTileSource.FilePathBitmapSource(mPath, 1024);
        } else {
            bitmapSource = new BitmapRegionTileSource.UriBitmapSource(this, imageUri, 1024);
        }
        setViewTileSource(bitmapSource, true, false, null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mTextureView.destroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(EXTRA_URL, mPath);
        System.out.println("onSaveInstanceState");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (null != savedInstanceState) {
            mPath = savedInstanceState.getString(EXTRA_URL);
        }
        System.out.println("onRestoreInstanceState" + mPath);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mTextureView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("onResume" + mPath);
        mTextureView.onResume();
    }

    public void setViewTileSource(final BitmapRegionTileSource.BitmapSource bitmapSource, final boolean touchEnabled,
                                  final boolean moveToLeft, final Runnable postExecute) {
        final Context context = FullscreenViewer.this;
        //final View progressView = findViewById(R.id.loading);
        final AsyncTask<Void, Void, Void> loadBitmapTask = new AsyncTask<Void, Void, Void>() {
            protected Void doInBackground(Void... args) {
                if (!isCancelled()) {
                    bitmapSource.loadInBackground();
                }
                return null;
            }

            protected void onPostExecute(Void arg) {
                if (!isCancelled()) {
                    //progressView.setVisibility(View.INVISIBLE);
                    if (bitmapSource.getLoadingState() == BitmapRegionTileSource.BitmapSource.State.LOADED) {
                        mTextureView.setTileSource(new BitmapRegionTileSource(context, bitmapSource), null);
                        mTextureView.setTouchEnabled(touchEnabled);
                        if (moveToLeft) {
                            mTextureView.moveToLeft();
                        }
                    }
                }
                if (postExecute != null) {
                    postExecute.run();
                }
            }
        };
        // We don't want to show the spinner every time we load an image, because that would be
        // annoying; instead, only start showing the spinner if loading the image has taken
        // longer than 1 sec (ie 1000 ms)
        /*progressView.postDelayed(new Runnable() {
            public void run() {
                if (loadBitmapTask.getStatus() != AsyncTask.Status.FINISHED) {
                    progressView.setVisibility(View.VISIBLE);
                }
            }
        }, 1000);*/
        loadBitmapTask.execute();
    }
}
